package com.leanagriassignment.retrofit;


import com.leanagriassignment.model.MovieResponseData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.leanagriassignment.retrofit.EndPoints.MOVIE_LIST;


public interface Api {
    @GET(MOVIE_LIST)
    Call<MovieResponseData> getMoviesList(@Query("page") int page, @Query("api_key") String apiKey, @Query("language") String language, @Query("region") String region);


}
