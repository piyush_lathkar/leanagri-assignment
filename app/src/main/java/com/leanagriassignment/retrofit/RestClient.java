package com.leanagriassignment.retrofit;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.leanagriassignment.retrofit.EndPoints.BASE_PROD_ONBOARD_URL;
import static com.leanagriassignment.util.AppConstant.REQUEST_TIME_OUT;

public class RestClient {
    public static RestClient instance = null;
    private static Api service;
    private OkHttpClient okHttpClient;

    private RestClient() {
        try {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
                    .readTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
                    .writeTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build();

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_PROD_ONBOARD_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            service = retrofit.create(Api.class);

        } catch (Exception e) {
            Log.e("Exception: ", "Error " + e.getMessage());
        }
    }

    public static RestClient getRestClient() {
        if (instance == null) {
            instance = new RestClient();
        }
        return instance;
    }

    public Api get() {
        return service;
    }
}
