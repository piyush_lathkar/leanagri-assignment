package com.leanagriassignment.util;

public interface AppConstant {
    long SPLASH_DISPLAY_TIME = 2 * 1000;
    int REQUEST_TIME_OUT = 300;
    int SUCCESS_RESPONSE = 200;

    String ARG_PARAM = "ARG_PARAM";
    String KEY_OPEN_VIEW = "KEY_OPEN_VIEW";
    String MOVIE_VIEW = "MOVIE_VIEW";
    String MOVIE_DETAILS_VIEW = "MOVIE_DETAILS_VIEW";
    String API_KEY = "443233abaa2ede7f6e7676d8494e7a7b";
    String LANGUAGE = "en-US";
    String REGION_FOR = "IN";
    String POSTER_BASE_URL = "http://image.tmdb.org/t/p/w185";
    String KAY_MOVIE_DETAILS = "KAY_MOVIE_DETAILS";

}
