package com.leanagriassignment.listener;

public interface OnSelectedSortListener {
    void selectedSort(int id);
}
