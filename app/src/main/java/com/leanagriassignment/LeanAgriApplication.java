package com.leanagriassignment;

import android.app.Application;
import android.os.StrictMode;

public class LeanAgriApplication extends Application {
    public static final String TAG = LeanAgriApplication.class.getSimpleName();
    private static LeanAgriApplication serviceManthraApplication = null;

    public static synchronized LeanAgriApplication getInstance() {
        return serviceManthraApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        serviceManthraApplication = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }
}
