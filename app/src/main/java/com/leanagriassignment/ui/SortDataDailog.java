package com.leanagriassignment.ui;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioGroup;

import com.leanagriassignment.R;
import com.leanagriassignment.listener.OnSelectedSortListener;

public class SortDataDailog extends DialogFragment {
    private View layoutView;
    private Dialog dialog;
    private OnSelectedSortListener onSelectedSortListener;
    private int id;
    private int selectedId;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layoutView = inflater.inflate(R.layout.dialog_sort, container, false);
        initView();
        return layoutView;
    }

    private void initView() {
        ((RadioGroup) layoutView.findViewById(R.id.rgBike)).check(selectedId);
        ((RadioGroup) layoutView.findViewById(R.id.rgBike)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                onSelectedSortListener.selectedSort(checkedId);
            }
        });
    }

    public void sendListener(OnSelectedSortListener onSelectedSortListener, int selectedId) {
        this.onSelectedSortListener = onSelectedSortListener;
        this.selectedId = selectedId;
    }
}
