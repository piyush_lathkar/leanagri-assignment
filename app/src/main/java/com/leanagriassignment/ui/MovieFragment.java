package com.leanagriassignment.ui;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.leanagriassignment.R;
import com.leanagriassignment.adapter.MovieAdapter;
import com.leanagriassignment.base.BaseActivity;
import com.leanagriassignment.base.BaseFragment;
import com.leanagriassignment.listener.OnDetialsClickedListener;
import com.leanagriassignment.listener.OnSelectedSortListener;
import com.leanagriassignment.model.MovieResponseData;
import com.leanagriassignment.retrofit.RestClient;
import com.leanagriassignment.roomdb.MovieData;
import com.leanagriassignment.roomdb.MovieDataBase;
import com.leanagriassignment.util.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.leanagriassignment.util.AppConstant.API_KEY;
import static com.leanagriassignment.util.AppConstant.ARG_PARAM;
import static com.leanagriassignment.util.AppConstant.KAY_MOVIE_DETAILS;
import static com.leanagriassignment.util.AppConstant.LANGUAGE;
import static com.leanagriassignment.util.AppConstant.MOVIE_DETAILS_VIEW;
import static com.leanagriassignment.util.AppConstant.REGION_FOR;
import static com.leanagriassignment.util.AppConstant.SUCCESS_RESPONSE;


public class MovieFragment extends BaseFragment implements OnSelectedSortListener, OnDetialsClickedListener {
    private SwipeRefreshLayout swipeView;
    private ProgressBar progressBar;
    private RecyclerView recMovie;
    private MovieAdapter movieAdapter;
    private List<MovieData> movieDataList = new ArrayList<>();
    private MovieDataBase movieDataBase;
    private int selectedId = R.id.rbRatinglh;
    private SortDataDailog dailog;
    private boolean isPagination;
    private int page = 1;

    public static MovieFragment newInstance(Bundle bundle) {
        MovieFragment fragment = new MovieFragment();
        Bundle args = new Bundle();
        args.putBundle(ARG_PARAM, bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentResource() {
        return R.layout.fragment_movie;
    }

    @Override
    protected void init(Bundle savedInstanceState, View layoutView) {
        movieDataBase = Room.databaseBuilder(getContext(), MovieDataBase.class, "movie_db").allowMainThreadQueries().build();
        recMovie = layoutView.findViewById(R.id.recMovie);
        swipeView = layoutView.findViewById(R.id.swipeView);
        progressBar = layoutView.findViewById(R.id.progressBar);
        recMovie.setLayoutManager(new GridLayoutManager(getContext(), 2));
        movieAdapter = new MovieAdapter(movieDataList, this);
        recMovie.setAdapter(movieAdapter);
        layoutView.findViewById(R.id.ivFilter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilterDialog();
            }
        });

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMovieData();
            }
        });

        getMovieData();
        recyclerAddScroll();

    }

    private void recyclerAddScroll() {
        recMovie.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!recyclerView.canScrollVertically(1)) {
                    if (isPagination) {
                        getMovieData();
                    }
                }
            }
        });
    }

    private void getMovieData() {
        if (Utils.hasInternetAccess(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RestClient.getRestClient().get().getMoviesList(page, API_KEY, LANGUAGE, REGION_FOR).enqueue(new Callback<MovieResponseData>() {
                @Override
                public void onResponse(Call<MovieResponseData> call, Response<MovieResponseData> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.code() == SUCCESS_RESPONSE) {
                        if (swipeView.isRefreshing()) {
                            swipeView.setRefreshing(false);
                        }
                        MovieResponseData movieResponseData = response.body();
                        // i api we are getting Total page ano and current page no ,then paggination requred up to current page< total page
                        isPagination = movieResponseData.getCurrentPage() < movieResponseData.getTotalPages();
                        // if you sroll next time then need to be send current page+1
                        page = movieResponseData.getCurrentPage() + 1;
                        movieDataList.addAll(movieResponseData.getMovieDataList());
                        movieDataBase.getMovieDao().clearData();
                        movieDataBase.getMovieDao().addAllMovies(movieDataList);
                        movieAdapter.notifyDataSetChanged();

                    } else {
                        //   todo handle error
                        showToast("Somthing went wrong");
                    }
                }

                @Override
                public void onFailure(Call<MovieResponseData> call, Throwable t) {
                    hideProgressDialog();
                    errorMsg(t);

                }
            });
        } else {
            movieDataList.clear();
            movieDataList.addAll(movieDataBase.getMovieDao().getMovieList());
            movieAdapter.notifyDataSetChanged();
        }
    }


    void openFilterDialog() {
        dailog = new SortDataDailog();
        dailog.sendListener(this, selectedId);
        dailog.setCancelable(true);
        dailog.show(getFragmentManager(), SortDataDailog.class.getName());
    }

    @Override
    public void selectedSort(int selectedId) {
        this.selectedId = selectedId;
        movieDataList.clear();
        dailog.dismiss();
        switch (selectedId) {
            case R.id.rbreleasDate:
                movieDataList.addAll(movieDataBase.getMovieDao().getMovieByReleaseDate());
                break;
            case R.id.rbRatinghl:
                movieDataList.addAll(movieDataBase.getMovieDao().getMovieByVoteCount());
                break;
            case R.id.rbRatinglh:
                movieDataList.addAll(movieDataBase.getMovieDao().getMovieByVoteCountDes());
                break;
        }
        movieAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDetails(int position) {
        Bundle bundle = new Bundle();
        bundle.putString(KAY_MOVIE_DETAILS, new Gson().toJson(movieDataList.get(position)));
        MoviesDetailFragment moviesDetailFragment = MoviesDetailFragment.newInstance(bundle);
        moviesDetailFragment.setArguments(bundle);

        ((BaseActivity) getActivity()).addOrReplaceFragment(moviesDetailFragment, MOVIE_DETAILS_VIEW, false);

    }
}
