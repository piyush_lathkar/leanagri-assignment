package com.leanagriassignment.ui;

import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.leanagriassignment.R;
import com.leanagriassignment.base.BaseFragment;
import com.leanagriassignment.roomdb.MovieData;
import com.leanagriassignment.util.Utils;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import static com.leanagriassignment.util.AppConstant.ARG_PARAM;
import static com.leanagriassignment.util.AppConstant.KAY_MOVIE_DETAILS;
import static com.leanagriassignment.util.AppConstant.POSTER_BASE_URL;

public class MoviesDetailFragment extends BaseFragment {
    private AppCompatImageView ivPoster;
    private AppCompatTextView tvMovieName;
    private AppCompatTextView tvOrinalLag;
    private AppCompatTextView tvMovieReleaseDate;
    private AppCompatRatingBar rbMovieRating;
    private AppCompatTextView tvOverDes;
    private AppCompatTextView tvTitle;
    private AppCompatCheckBox cbAdult;

    public static MoviesDetailFragment newInstance(Bundle bundle) {
        MoviesDetailFragment fragment = new MoviesDetailFragment();
        Bundle args = new Bundle();
        args.putBundle(ARG_PARAM, bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentResource() {
        return R.layout.fragment_movie_details;
    }

    @Override
    protected void init(Bundle savedInstanceState, View layoutView) {
        MovieData movieData = new Gson().fromJson(getArguments().getString(KAY_MOVIE_DETAILS, null), MovieData.class);
        layoutView.findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        if (movieData != null) {
            // find id
            tvTitle = layoutView.findViewById(R.id.tvTitle);
            tvOverDes = layoutView.findViewById(R.id.tvOverDes);
            tvOrinalLag = layoutView.findViewById(R.id.tvOrinalLag);
            ivPoster = layoutView.findViewById(R.id.ivPoster);
            tvMovieName = layoutView.findViewById(R.id.tvMovieName);
            tvMovieReleaseDate = layoutView.findViewById(R.id.tvMovieReleaseDate);
            rbMovieRating = layoutView.findViewById(R.id.rbMovieRating);
            cbAdult = layoutView.findViewById(R.id.cbAdult);


            // swe can set Data Direct also ==> ((AppCompatTextView)layoutView.findViewById(R.id.tvTitle)).setText("set text");
            tvTitle.setText(movieData.getOriginalTitle());
            tvOrinalLag.setText(movieData.getOriginalLanguage());
            cbAdult.setChecked(movieData.getAdult());
            tvOverDes.setText(movieData.getOverview());
            tvMovieName.setText(movieData.getTitle());
            rbMovieRating.setRating(movieData.getVoteAverage()/2);   // ratted out 10 , but we are showing out of 5
            Log.d("Rating====>", "" + rbMovieRating.getRating());
            tvMovieReleaseDate.setText(movieData.getReleaseDate());
            String fullPosterUrlPath = POSTER_BASE_URL + movieData.getPosterPath();
            Log.d("path", fullPosterUrlPath);
            if(Utils.hasInternetAccess(getContext())) {
                Picasso picasso = Picasso.with(getContext());
                picasso.setLoggingEnabled(true);
                picasso.load(fullPosterUrlPath)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .centerCrop()
                        .fit()
                        .error(R.drawable.ic_place_holder)
                        .into(ivPoster);
            }
        }
    }
}
