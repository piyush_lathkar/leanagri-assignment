package com.leanagriassignment.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.widget.Toolbar;

import com.leanagriassignment.R;
import com.leanagriassignment.base.BaseActivity;

import static com.leanagriassignment.util.AppConstant.KEY_OPEN_VIEW;
import static com.leanagriassignment.util.AppConstant.MOVIE_DETAILS_VIEW;
import static com.leanagriassignment.util.AppConstant.MOVIE_VIEW;

public class ActionActivity extends BaseActivity {

    @Override
    protected int getContentResource() {
        return R.layout.activity_action;
    }

    @Override
    protected void init(@Nullable Bundle state) {
        String openView = getIntent().getStringExtra(KEY_OPEN_VIEW);
        switch (openView) {
            case MOVIE_VIEW:
                // todo if want pass the data from to respective fragment pass into bundle
                addOrReplaceFragment(MovieFragment.newInstance(new Bundle()),openView,true);
                break;
           /* case MOVIE_DETAILS_VIEW:
                addOrReplaceFragment(MovieFragment.newInstance(new Bundle()),openView,false);
                break;*/
        }
    }
}
