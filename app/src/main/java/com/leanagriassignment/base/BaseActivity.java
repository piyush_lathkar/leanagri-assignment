package com.leanagriassignment.base;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.leanagriassignment.R;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentResource());
        init(savedInstanceState);
    }

    @LayoutRes
    protected abstract int getContentResource();

    protected abstract void init(@Nullable Bundle state);

    public void addOrReplaceFragment(final Fragment fragment, final String tag, final boolean isReplacce) {
        try {
            if (!isFinishing()) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        if (!fragmentManager.isStateSaved() && fragmentManager.findFragmentByTag(tag) == null) {
                            if (isReplacce) {
                                fragmentTransaction.replace(R.id.rootContainer, fragment, tag);
                            } else {
                                fragmentTransaction.add(R.id.rootContainer, fragment, tag);
                                fragmentTransaction.addToBackStack(tag);
                            }
                            fragmentTransaction.commitAllowingStateLoss();
                            fragmentManager.executePendingTransactions();
                        } else {
                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        }
                    }
                });
            }
        } catch (Exception ignored) {

        }
    }
}
