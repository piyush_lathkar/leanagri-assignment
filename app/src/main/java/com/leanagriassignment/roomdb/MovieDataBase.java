package com.leanagriassignment.roomdb;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {MovieData.class},version = 1,exportSchema = false)
public abstract class MovieDataBase extends RoomDatabase {
    public abstract MovieDao getMovieDao();
}
